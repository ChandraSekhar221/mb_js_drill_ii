function titleCaseFullName (nameObject) {
    if (Object.keys(nameObject).length === 2) {
        let firstName = nameObject.first_name.toLowerCase()
        firstName = firstName[0].toUpperCase() + firstName.slice(1)
        let lastName = nameObject.last_name.toLowerCase()
        lastName = lastName[0].toUpperCase() + lastName.slice(1)
        return firstName + ' ' + lastName
    }
    if (Object.keys(nameObject).length === 3) {
        let firstName = nameObject.first_name.toLowerCase()
        firstName = firstName[0].toUpperCase() + firstName.slice(1)
        let middleName = nameObject.middle_name.toLowerCase()
        middleName = middleName[0].toUpperCase() + middleName.slice(1)
        let lastName = nameObject.last_name.toLowerCase()
        lastName = lastName[0].toUpperCase() + lastName.slice(1)
        return firstName + ' ' + middleName + ' ' + lastName
    }
    
}

module.exports = titleCaseFullName 