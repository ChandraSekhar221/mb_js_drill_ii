function convertToStringFromArray (arrayOfStrings) {
    return arrayOfStrings.join(" ")
}

module.exports = convertToStringFromArray 