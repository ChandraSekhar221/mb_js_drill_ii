function conversionIntFromString(array) {
    intArray = []
    for (let i = 0 ; i < array.length ; i++ ) {
        let currency = array[i]
        let num = ""
        for (let j = 0 ; j < currency.length ; j++) {
            if (!( currency[j] === '$' || currency[j] === ',' )) {
                num += currency[j]
            }
        }
        num = parseFloat(num)
        intArray.push(num)
    }
    return intArray
}

module.exports = conversionIntFromString ;