function findMonth (dateString) {
    let indexOfFirstSlash = dateString.indexOf('/') + 1
    let indexOfLatSlash = dateString.lastIndexOf('/')
    let month = dateString.slice( indexOfFirstSlash,indexOfLatSlash)
    return month
}

module.exports = findMonth 